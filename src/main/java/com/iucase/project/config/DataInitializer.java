package com.iucase.project.config;

import com.iucase.project.entity.Funcionario;
import com.iucase.project.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    FuncionarioRepository funcionarioRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

    }

    public void createFuncionario(String nome, Integer chave_equipe, String cargo, String papel, Integer idade) {
        Funcionario funcionario = new Funcionario(nome, chave_equipe, cargo, papel, idade);
        funcionarioRepository.save(funcionario);
    }
}
