package com.iucase.project.repository;

import com.iucase.project.entity.Funcionario;
import com.sun.xml.bind.v2.model.core.ID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

    @Query("SELECT f FROM Funcionario f WHERE f.id_equipe = ?1")
    List<Funcionario> findByEquipe(Integer Equipe);

    @Query("SELECT f FROM Funcionario f WHERE f.id = ?1")
    Funcionario findById(ID id);
}
