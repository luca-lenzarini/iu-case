package com.iucase.project.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Setter
@Getter
public class Funcionario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private Integer id_equipe;
    private String cargo;
    private String papel;
    private Integer idade;

    public Funcionario(){}

    public Funcionario(String nome, Integer id_equipe, String cargo, String papel, Integer idade) {
        this.nome = nome;
        this.id_equipe = id_equipe;
        this.cargo = cargo;
        this.papel = papel;
        this.idade = idade;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setId_equipe(Integer id_equipe) {
        this.id_equipe = id_equipe;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public void setPapel(String papel) {
        this.papel = papel;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Integer getId_equipe() {
        return id_equipe;
    }

    public String getCargo() {
        return cargo;
    }

    public String getPapel() {
        return papel;
    }

    public Integer getIdade() {
        return idade;
    }
}
