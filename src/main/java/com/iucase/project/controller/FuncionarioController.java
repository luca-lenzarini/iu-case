package com.iucase.project.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.iucase.project.entity.Funcionario;
import com.iucase.project.model.GetUserModel;
import com.iucase.project.repository.FuncionarioRepository;
import org.apache.coyote.Response;
import org.hibernate.validator.internal.constraintvalidators.bv.time.future.AbstractFutureEpochBasedValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/funcionarios")
public class FuncionarioController {

    @Autowired
    FuncionarioRepository funcionarioRepository;

    /*
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Funcionario> getAllFuncionarios(){
        return funcionarioRepository.findAll();
    }

     */

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Object> postUser(@RequestBody Funcionario funcionario){

        if(
            funcionario.getNome() == null ||
            funcionario.getId_equipe() == null ||
            funcionario.getCargo() == null ||
            funcionario.getPapel() == null ||
            funcionario.getIdade() == null
        ) {
            return new ResponseEntity<Object>(
                new GenericResponse(
                    "error", 0, "Verifique o preenchimento dos campos"
                ),
                HttpStatus.OK
            );
        }

        funcionarioRepository.save(funcionario);
        return new ResponseEntity<Object>(funcionario, HttpStatus.OK);
    }

    @GetMapping(value = {"/{id}", ""})
    public ResponseEntity<Object> getFuncionario(@PathVariable(required = false) Long id){
        if(id != null) {
            if (funcionarioRepository.existsById(id))
                return new ResponseEntity<Object>(funcionarioRepository.findById(id), HttpStatus.OK);
        }else {
            List<Funcionario> funcionarios = funcionarioRepository.findAll();

            if(!funcionarios.isEmpty())
                return new ResponseEntity<Object>(funcionarios, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value= {"/{id}", ""})
    public ResponseEntity<Object> deleteFuncionario(@PathVariable(required = true) Long id) {

        if(!funcionarioRepository.existsById(id)) {
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        }

        funcionarioRepository.deleteById(id);

        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @PutMapping(value= {"/{id}", ""})
    public ResponseEntity<Object> updateFuncionario(
            @PathVariable(required = true) Long id,
            @RequestBody(required = true) Funcionario newFuncionario
    ){
        if(!funcionarioRepository.existsById(id)) {
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Object>(funcionarioRepository.findById(id)
            .map(funcionario -> {
                funcionario.setNome(newFuncionario.getNome());
                funcionario.setCargo(newFuncionario.getCargo());
                funcionario.setId_equipe(newFuncionario.getId_equipe());
                funcionario.setIdade(newFuncionario.getIdade());
                funcionario.setPapel(newFuncionario.getPapel());
                return funcionarioRepository.save(funcionario);
            })
            .orElseGet(() -> {
                newFuncionario.setId(id);
                return funcionarioRepository.save(newFuncionario);
            }), HttpStatus.OK);
    }

    private class GenericResponse {
        private Timestamp timestamp;
        private String error;
        private Integer status;
        private String message;

        GenericResponse(String error, Integer status, String message) {
            this.timestamp = new Timestamp(System.currentTimeMillis());
            this.error = error;
            this.status = status;
            this.message = message;
        }

        public Timestamp getTimestamp() {
            return timestamp;
        }

        public String getError() {
            return error;
        }

        public Integer getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }
    }
}
